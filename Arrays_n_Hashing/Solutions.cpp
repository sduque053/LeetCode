#include "Solutions.hpp"

using namespace std;


Solution::Solution(void){
    cout<<"Solution object created\n";
}

bool Solution::containsDuplicate_brute(const vector<int>& nums) {
    
    
       
    vector<int>::const_iterator   ix1;
    vector<int>::const_iterator   ix2;

    ix1 = nums.begin();

    for (ix1; ix1<nums.end(); ix1++){
        for (ix2=ix1+1; ix2<nums.end(); ix2++){

            if( *ix1==*ix2){

                return true;
            }

        }
    }

    return false;
        
         
}

bool Solution::containsDuplicate(const vector<int>& nums) {
    unordered_set<int> seen;
    vector<int>::const_iterator ix1 = nums.begin();

    for (ix1; ix1<nums.end(); ix1++) {
        if (seen.find(*ix1) != seen.end()) {
            return true;
        }
        seen.insert(*ix1);
    }
    
    return false;
}

vector<int> Solution::twoSum(const vector<int>& nums,const int target) {

    unordered_map<int, int> map;
    vector<int> result;

    vector<int>::const_iterator ix1 = nums.begin();

    int complement;
    int index;

    for(ix1; ix1<nums.end();ix1++){
        complement = target-*ix1;
        index = static_cast<int>( ix1 - nums.begin() );

        if(map.find(complement) != map.end()){
            result.push_back(index);
            result.push_back(map[complement]);

            break;
        } else{

            map.insert({*ix1, index});

        }
    }

    return result;

}

//First blind proposed solution
bool Solution::isAnagram_V1(const string s, const string t){

    unordered_map<char, int> map;

    string::const_iterator ix1 = s.begin();

    for (ix1;ix1<s.end();ix1++){
        if(map.find(*ix1)!=map.end()){
            map[*ix1] += 1;
        }else{
            map.insert({*ix1,1});
        }
    }

    ix1 = t.begin();

    for (ix1;ix1<t.end();ix1++){
        if(map.find(*ix1)!=map.end()){
            map[*ix1] -= 1;
            
            if(map[*ix1]==0){
                map.erase(*ix1);
            }

        }else{
            return false;
        }


    }

    if(!map.empty()){
        return false;
    }

    return true;

}

//Different solutionm, with a faster runtime
bool Solution::isAnagram_V2(const string s, const string t){

    if(s.size() != t.size()) return false;

    unordered_map<char, int> s_map, t_map;
    
    for (int ix1=0;ix1<s.size();ix1++){
        s_map[s[ix1]]++;
        t_map[t[ix1]]++; 
    }



    for (auto ix2=s_map.begin();ix2!=s_map.end();ix2++){

        if(s_map[ix2->first] != t_map[ix2->first]){
            return false;
        }

    }

    return true;

}


