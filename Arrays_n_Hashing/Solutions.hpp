#include <vector>
#include <cstdlib>
#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <string>


using namespace std;

class Solution {
public:
    Solution(void);

    bool containsDuplicate_brute(const vector<int>& nums);
    bool containsDuplicate(const vector<int>& nums);

    vector<int> twoSum(const vector<int>& nums, const int target);

    bool isAnagram_V1(const string s, const string t);
    bool isAnagram_V2(const string s, const string t);



};