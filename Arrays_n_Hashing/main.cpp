#include<vector>
#include<cstdlib>
#include<iostream>

#include "Solutions.hpp"

using namespace std;

int main(void){

    vector<int> TestVect {1,2,3,1};
    bool duplicates_y_n;

    Solution Dupes;

    duplicates_y_n = Dupes.containsDuplicate(TestVect);

    if(!duplicates_y_n){
        cout<<"false";
        return 0;
    }

    cout<<"true";

    return 0;

}