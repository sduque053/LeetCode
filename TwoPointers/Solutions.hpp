#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <unordered_map>
#include <locale>         // std::locale, std::tolower

using namespace std;

class Solution {
public:
    Solution(void);

    bool isPalindrome_sanitized(string s_in);
    bool isPalindrome_inwhile(string s);


};