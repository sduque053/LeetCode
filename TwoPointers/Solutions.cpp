#include "Solutions.hpp"

using namespace std;

Solution::Solution(void){
    cout<<"Solution object created\n";
}

bool Solution::isPalindrome_sanitized(string s_in) {
    
    if(s_in.size()==0 || s_in.size()==1) return true;

    string s;

    for (int i=0;i<s_in.size();i++){
        if(!isalnum(s_in[i])){
            continue;
        }else if(isupper(s_in[i])){
            s+=static_cast<char>( tolower(s_in[i]) );
        }else{
            s+=s_in[i];
        }
    }

    string::iterator it1 = s.begin();
    string::reverse_iterator it2 = s.rbegin();

    while( (it1!=s.end()) || (it2!=s.rend()) ){

        if(*it1 != *it2) return false;

        if( &*it1 == &*it2 ) break;//no need to go all the way to end if the pointers meet without errors its a palindrome

        it1++;
        it2++;
    }

    return true;
}

bool Solution::isPalindrome_inwhile(string s) {


    if(s.size()==0 || s.size()==1) return true;

    string::iterator it1 = s.begin();
    string::reverse_iterator it2 = s.rbegin();

    while( (it1!=s.end()) && (it2!=s.rend()) ){

        while( (it1 != (s.end())) && !(isalnum(*it1)) ) it1++;
        while( (it2 != (s.rend())) && !(isalnum(*it2)) ) it2++;

        if(it1 == s.end() || it2 == s.rend() ) return true;

        if( isupper(*it1) ) *it1=static_cast<char>(tolower(*it1));
        if( isupper(*it2) ) *it2=static_cast<char>(tolower(*it2));
        if( (*it1 != *it2) ) return false; 

        if( &*it1 == &*it2 ) break;
        if( &*it1 - &*it2 == 1) break;

        it1++;
        it2++;

    }

    return true;
}