#include <string>
#include <iostream>

#include "Solutions.hpp"

using namespace std;

int main(void){

    string test_s="Never a foot too far, even."; 
    Solution SLT;

    cout<<SLT.isPalindrome_inwhile(test_s);

    return 0;

}