#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <unordered_map>

using namespace std;

class Solution {
public:
    Solution(void);

    bool Parentheses_isValid_hashmap(string s);
    bool Parentheses_isValid        (string s);

};