#include "Solutions.hpp"

using namespace std;

Solution::Solution(void){
    cout<<"Solution object created\n";
}

bool Solution::Parentheses_isValid(string s){

    stack<char> OpnQ; //Track of: ( , {, [
    int irrlvnt=0; //Track of others

    OpnQ.push('!');

    for(auto it=s.begin();it!=s.end();it++){

        switch(*it){

            case '(':
                OpnQ.push(*it);

                break;
            case '{':
                OpnQ.push(*it);
                break;
            case '[':
                OpnQ.push(*it);
                break;

            case ')':
                if( OpnQ.top() != '(') return false;
                OpnQ.pop();
                break;
            case '}':
                if( OpnQ.top() != '{') return false;
                OpnQ.pop();
                break;
            case ']':
                if( OpnQ.top() != '[') return false;
                OpnQ.pop();
                break;

            default:
                irrlvnt++;
                break;            
        }
        
    }

    if( OpnQ.top() == '!' ) return true;

    return false;

}

bool Solution::Parentheses_isValid_hashmap(string s) {

    stack<char> OpnQ;

    unordered_map<char, char> parens = {
        {')', '('},
        {']', '['},
        {'}', '{'},
    };
    
    for (auto it=s.begin();it!=s.end();it++) {
        if (parens.find(*it) != parens.end()) {
            // if input starts with a closing bracket.
            if (OpnQ.empty()) {
                return false;
            }

            if (OpnQ.top() != parens[*it]) {
                return false;
            }

            OpnQ.pop();
        } else {
            OpnQ.push(*it);
        }
    }
    
    return OpnQ.empty();

}
